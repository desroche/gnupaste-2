# GNU Generation pastebin

## History

This used to be a webapp in `yesod`; except that was massively overkill and
unmaintainable, so one day I got bored of writing java and rewrote the whole
thing using `servant`.

## Usage

## Implementation choices

 * The programming language is [haskell][haskell]. There are several reasons
   why it was chosen: this app was historically in haskell, alongside a bunch
   of other GNU Generation-related programs. The author is also quite partial
   towards this language, mainly for it's strict functional paradigm, as well
   as it's powerful type system that allow nice things, including extensive
   compile-time verifications.

 * The web framework chosen is [servant][haskell-servant]. It specializes in
   writing REST APIs, specifying said APIs as types, allowing very strict
   checking at compile-time of coherence and easy testing. It also implements
   all the conversions between untyped values returned to the outside world and
   inner types, as well as abstracting away most of the crappy web stuff. It
   has, over the anciently-used [yesod][haskell-yesod] the advantage of being
   lightweight and less opinionated towards huge webapps with a lot of template
   Haskell and advanced stuff useless in this case.

 * Plain text is written and served using [blaze-html][haskell-blaze],
   a Haskell-embedded DSL for writing html. This allows the values written to
   be checked and generated at compile-time, while still having the ability to
   modify these templates through function composition.

 * The database is [SQLite][sqlite-database]. Since we are only storing one
   table, selecting on either the `hash` or `expiration` fields, this small
   file-based database is adapted. This also eases deploy as the SQLite Haskell
   binding library we use is self-contained.

## Implementation details

The program is organized in the following way:

 * `src/Paste/API.hs` defines the API.
 * `src/Paste/Handlers.hs` defines the logic for the server.
 * `src/Paste/Homepage.hs` contains the `blaze` code for the homepage.
 * `src/Paste/Database.hs` contains the code to interact with the database.
 * `src/Paste/Utils.hs` contains various small utilities.

[haskell-blaze]: https://hackage.haskell.org/package/blaze-markup
[haskell-servant]: https://github.com/haskell-servant/servant
[haskell-sqlite]: https://hackage.haskell.org/package/sqlite-simple
[haskell-yesod]: https://www.yesodweb.com
[haskell]: https://haskell.org
[sqlite-database]: https://sqlite.org
