{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : Paste.Homepage
Description : This module contains the code to generate the HTML for the homepage.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

This module contains the code for the generation of the homepage, written in the "Text.Blaze.Html5" EDSL.
-}

module Paste.Homepage where

import Text.Blaze.Html5            as H
import Text.Blaze.Html5.Attributes as A

-- | The homepage that serves as an instruction manual, and a post page for
-- people using browsers.
homepage :: Html
homepage = docTypeHtml $ do
    H.head $
             H.title "Paste"
    H.body $ do
             h1 "Paste"

             h2 "Usage:"
             pre "<command> | curl -F 'file=@-' -H -Expect:' https://paste.gnugen.ch"

             h2 "Details"
             p $ do
                " The file field is the only required field. It is the file that"
                " will be uploaded and served. The name field can be used to"
                " rename the file otherwise than it's name on-disk; the mime-type"
                " to force a different mime-type than the one recognized by the"
                " service; and finally the expiration to vary the default"
                " expiration period of one year. Valid units are s(econds),"
                " h(ours), d(ays), w(eeks), m(onths)."

             p $ do
                "See the "
                a ! href "https://gitlab.gnugen.ch/gnugen/pastebin" $ "gitlab page"
                " for more information on the implementation of this service."
             p $ do
                 " This service is provided with no guarantees of any kind. Please note"
                 " that your IP address will be stored for security purposes."

             h2 "Upload form"
             H.form ! A.method "POST" $ do
                table $ do
                    tr $ do
                         td "Select file to upload"
                         td $ input ! A.type_ "file" ! A.name "file" ! A.required "file"
                    tr $ do
                         td "Rename to (leave blank to keep as is)"
                         td $ input ! A.type_ "text" ! A.name "filename"
                    tr $ do
                         td "Force mimetype (leave blank to keep as is)"
                         td $ input ! A.type_ "text" ! A.name "type"
                    tr $ do
                         td "Expiration date (leave blank for default of one year)"
                         td $ input ! A.type_ "text" ! A.name "expire"
                input ! A.type_ "submit" ! A.formenctype "multipart/form-data"
             footer $ do
                      "Paste v2, BSD3 licenced. Written in "
                      a ! href "https://haskell.org" $ "haskell"
                      " using "
                      a ! href "https://hackage.haskell.org/package/servant" $ "servant"
                      " by J. Desroches for GNU Generation, 2018. Built on the"
                      " old paste program in Yesod by A. Angel, 2014"

