{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

{-|
Module      : Paste.Handlers
Description : gnupaste's logic
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

In this module, the functions that actually answer requests made to API
endpoints are defined.
-}

module Paste.Handlers
    ( server
    )
where

import           Protolude

import           Paste.API
import           Paste.Database
import           Paste.Homepage
import           Paste.Utils

import qualified Data.Text            as T
import           Data.Time.Calendar
import           Data.Time.Clock
import           Database.Persist.Sql
import           Network.Socket
import           Servant
import           Servant.Multipart
import           Servant.Server
import           System.Directory
import           Text.Read
import           Text.Regex

-- | Server implementing our API. Contains all the logic.
server :: ConnectionPool  -- ^ Database pool.
       -> FilePath        -- ^ Directory containing uploads.
       -> Server PasteAPI
server pool uploads = return homepage
                 :<|> serveDirectoryWebApp uploads
                 :<|> processUpload pool uploads

-- | Process a file upload request to our server.
processUpload :: ConnectionPool     -- ^ Database pool.
              -> FilePath           -- ^ Directory containing uploads.
              -> SockAddr           -- ^ Address of the client making the request.
              -> Maybe Text         -- ^ Optional 'X-Forwarded-For' header.
              -> MultipartData Tmp  -- ^ Data uploaded in the "multipart/formdata".
              -> Handler Text
processUpload pool uploads reqAddr mXForward multipData =
    -- Process and check request: first and foremost, do we have a file ?
    case head $ files multipData of
        Nothing     -> throwError err400 { errBody = "You must include a file in the multipart/formdata you upload!" }
        Just upload -> do
    -- Get information to store.
            now    <- liftIO getCurrentTime

            -- Generate a hash for the file, unique wrt the DB.
            hash   <- liftIO $ generateHash pool

            -- Get the file name, mimetype and expiration date from defaults or user given data.
            name   <- parseField (return $ fdFileName upload) "filename" return multipData
            mime   <- parseField (return $ fdFileCType upload) "type" return multipData
            expire <- parseField (return $ addYear now) "expire" (parseTimeDiff now) multipData

            -- Address from X-Forwarded-For header if it exists, otherwise from the requester.
            let address = fromMaybe (textifyAddress reqAddr) (mXForward)

    -- copy file to "uploads" dir.
            liftIO $ copyFile (fdPayload upload) (uploads ++ (T.unpack hash))

    -- Store info in DB.
            liftIO $ insertPaste Paste { pasteHash  = hash
                                       , pasteName  = name
                                       , pasteMime  = mime
                                       , pasteDate  = now
                                       , pasteDueAt = expire
                                       , pasteIpAdd = address
                                       } pool
    -- Send back 201 with the URL to the paste.
            return $ "https://paste.gnugen.ch/" <> hash

-- | Transform a 'SockAddr' datastructure to human-readable text to store in
-- the database.
textifyAddress :: SockAddr -- ^ The address to 'Text'ify.
               -> Text
textifyAddress (SockAddrInet _ host)      = T.pack . show . hostAddressToTuple $ host
textifyAddress (SockAddrInet6 _ _ host _) = T.pack . show $ host
textifyAddress  _                         = "Address not found"

-- | Parse a field from the form, given a default value and a parser.
parseField :: Handler a             -- ^ The default value to return if parsing fails or the field is empty.
           -> Text                  -- ^ The name of the field to extract and parse.
           -> (Text -> Handler a)   -- ^ A parser for the field.
           -> MultipartData Tmp     -- ^ The "multipart/formdata" data to extract from.
           -> Handler a
parseField defValue fieldName parser multipData = maybe defValue parseField (lookupInput fieldName multipData)
    where parseField "" = defValue
          parseField fv = parser fv

-- | Add one year to a 'UTCTime', rounding to midnight.
addYear :: UTCTime -- ^ The time to add to.
        -> UTCTime
addYear now = UTCTime (addGregorianYearsRollOver 1 (utctDay now)) 0

-- | Parse the `expire` field and return the time with the expiry duration
-- added. The field is expected to contain numbers followed by one of s, h, d,
-- w or m, with the obvious unit values associated.
parseTimeDiff :: UTCTime            -- ^ The current time.
              -> Text               -- ^ The data in the expiry field.
              -> Handler UTCTime
parseTimeDiff now input = do
    let reg = mkRegex "^([0-9]+)([shdwm])?$"
    seconds <- case matchRegex reg (T.unpack input) of
                    Just (i:u:_) -> return $ (read i) * (parseUnit u)
                    _            -> throwError err400 { errBody = "Invalid expiration date specified" }
    return $ addUTCTime (fromIntegral seconds) now

-- | Return the number of seconds contained in the given unit.
parseUnit :: [Char] -- ^ One of s, h, d, w, m for the obvious associated time units.
          -> Int
parseUnit "s" = 1              -- second
parseUnit "h" = 3600           -- hour
parseUnit "d" = 3600 * 24      -- day
parseUnit "w" = 3600 * 24 * 7  -- week
parseUnit "m" = 3600 * 24 * 30 -- month
parseUnit  _  = 1              -- no unit means 1 second
