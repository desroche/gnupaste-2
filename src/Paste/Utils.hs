{-# LANGUAGE NoImplicitPrelude #-}

{-|
Module      : Paste.Utils
Description : gnupaste's utility functions
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX
-}

module Paste.Utils where

import Protolude

import Data.List
import System.Random

-- | List of available characters for creating the hash.
pasteChars :: [Char]
pasteChars = ['a'..'z'] ++ ['A'..'Z'] ++ ['0'..'9']

-- | Pick an random element from a list.
pickElem :: [a]  -- ^ The list to pick from.
         -> IO a
pickElem xs = (xs !!) <$> randomRIO (0, length xs - 1)
