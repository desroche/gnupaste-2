{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE TypeOperators     #-}

{-|
Module      : Paste.API
Description : gnupaste's API.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

In this module can be found the type declaration describing our API.
-}

module Paste.API
    (PasteAPI)
where

import Protolude

import Servant
import Servant.API.Header
import Servant.API.RemoteHost
import Servant.API.Verbs
import Servant.HTML.Blaze
import Servant.Multipart
import Text.Blaze.Html5

-- | Type-level description of our API.
--
-- A user can either get the homepage with instructions, upload a file with the
-- given form, or ask for a file using it's hash.
type PasteAPI = Get '[HTML] Html
           :<|> "raw" :> Raw
           :<|> RemoteHost
                    :> Header "X-Forwarded-For" Text
                    :> MultipartForm Tmp (MultipartData Tmp)
                    :> PostCreated '[PlainText] Text
