{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

{-|
Module      : Paste.Database
Description : gnupaste's functions to interact with the database
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

-}

module Paste.Database where

import           Protolude

import           Paste.Utils

import           Control.Concurrent
import qualified Data.Text            as T
import           Data.Time.Clock
import           Database.Persist.Sql
import           Database.Persist.TH
import           System.Directory

-- Define schema and migration for our database, using template haskell.
share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
Paste sql=pastes
    hash  Text
    name  Text
    mime  Text
    date  UTCTime
    dueAt UTCTime
    ipAdd Text
        UniqueHash hash
        deriving Show Read Eq
|]

-- | Insert a 'Paste' into the database connected to a pool.
insertPaste :: Paste            -- ^ The 'Paste' to input.
            -> ConnectionPool   -- ^ The 'ConnectionPool' to the database.
            -> IO ()
insertPaste = runSqlPool . insert_

-- | Generate a 4+ letter random hash, checking against the database for
-- unicity.
generateHash :: ConnectionPool -- ^ The 'ConnectionPool' to the database.
             -> IO Text
generateHash pool = do
    hash <- forM [1 .. 4] . const $ pickElem pasteChars
    checkHash pool hash

    -- Inner recursive function that appends a character upon each collision.
    where checkHash :: ConnectionPool -> [Char] -> IO Text
          checkHash pool hash = do
                mExists <- runSqlPool (selectFirst [PasteHash ==. T.pack hash] []) pool
                case mExists of Nothing -> return $ T.pack hash
                                Just h  -> pickElem pasteChars >>= checkHash pool . (: hash)

-- | Function to cleanup expired pastes from the database and drive. Do not
-- call outside of the main loop: this function does not terminate!
cleanupPastes :: ConnectionPool -- ^ The 'ConnectionPool' to the database
              -> FilePath       -- ^ The 'FilePath' to the directory containing uploads.
              -> IO ()
cleanupPastes pool uploads = do
    -- Obtain the list of expired pastes.
    now   <- getCurrentTime
    files <- runSqlPool (selectList [PasteDueAt <. now] []) pool

    -- Remove the files and DB references.
    sequence_ $ (processFiles pool uploads) <$> files

    -- Wait and recurse.
    threadDelay $ 3600 * 24 * 1000000 -- once every day
    cleanupPastes pool uploads

    -- Remove files from the database and then remove them.
    where processFiles :: ConnectionPool -> FilePath -> Entity Paste -> IO ()
          processFiles pool uploads (Entity key paste) =
            runSqlPool (delete key) pool >> removeFile (uploads ++ T.unpack (pasteHash paste))
